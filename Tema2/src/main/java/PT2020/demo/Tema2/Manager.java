package PT2020.demo.Tema2;

import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.List;
import java.io.File;
import java.io.FileWriter;

public class Manager extends Thread {
	private int simulationTime;
	private List<Shop> shops;
	private Queue queue;

	private File out;
	private FileWriter myWriter; 

	public Manager(int simulationTime, List<Shop> shops, Queue queue) {
		this.simulationTime = simulationTime;
		this.shops = shops;
		this.queue = queue;

		this.queue.generateClients();
		this.queue.sortClients();

		createOutputFile();
	}

	public void createOutputFile() {
		try {
			
			File out = new File("Out-Test.txt");
		    myWriter = new FileWriter(out); 
			if (out.createNewFile()) {
				System.out.println("File created: " + out.getName());
			} else {
				System.out.println("File already exists.");
			}
		} catch (IOException e) {
			System.out.println("An error occurred.");
			e.printStackTrace();
		}
	}

	// Send client to the shop with fewer number of clients queued.
	public void dispatchClient(Client c) {
		Shop minShop = null;
		int minSize = Integer.MAX_VALUE;

		for (Shop s : shops) {
			int size = s.getQueueSize();
			if (size < minSize) {
				minSize = size;
				minShop = s;
			}
		}
		if (minShop != null) {
			minShop.addClient(c);
		}
	}

	public void writeToOutput(int time, List<Client> scheduledClients)  {

		try {
		
		myWriter.write("Time " + time);
		myWriter.write("\n");

		String res = new String();
		for (Client c : scheduledClients) {
			res += c.toString() + "; ";
		}
		myWriter.write("Waiting clients: " + res);
		myWriter.write("\n");
		for (Shop s : shops) {
			myWriter.write(s.toString());
			myWriter.write("\n");
		}
		
		 } catch (IOException e) {
		      System.out.println("An error occurred.");
		      e.printStackTrace();
		    }
	}

	public void run() {
		for (int time = 0; time < simulationTime; time++) {
			List<Client> waitingClients = new ArrayList<Client>();
			for (Client c : queue.getClients()) {
				if (c.getArrivalTime() == time) {
					dispatchClient(c);
				}
				if (c.getArrivalTime() > time) {
					waitingClients.add(c);
				}
			}

			writeToOutput(time, waitingClients);

			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			myWriter.flush();
			myWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
