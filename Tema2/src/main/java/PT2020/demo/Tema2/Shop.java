package PT2020.demo.Tema2;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

public class Shop extends Thread {
  private int id;
	private LinkedBlockingQueue<Client> clients;
  private AtomicInteger waitingPeriod;

	public Shop(int i) {
    id = i;
    clients = new LinkedBlockingQueue<Client>();
    waitingPeriod = new AtomicInteger(0);
	}

  public void addClient(Client c) {
    clients.add(c);
    waitingPeriod.incrementAndGet();
  }

  public void run() {
    while(true) {
      Client c = clients.peek();
      if (c != null) {
        try {
          Thread.sleep(c.getServiceTime()* 1000);
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
        clients.poll();
        waitingPeriod.decrementAndGet();
      }
    }
  }

  public int getQueueSize() {
    return clients.size();
  }

  public String toString() {
    String res = "Queue " + id + ": ";

    if (waitingPeriod.get() == 0) {
      res += "closed";
    } else {
      for (Client c : clients) {
        res += c + ",";
      }
    }

    return res;
  }
}
