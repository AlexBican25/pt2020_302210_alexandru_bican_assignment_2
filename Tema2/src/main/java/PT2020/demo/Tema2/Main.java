package PT2020.demo.Tema2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	private static int max_service_time;
	private static int min_service_time;
	private static int max_arrival_time;
	private static int min_arrival_time;
	private static int noClients;
	private static int noShops;
	private static int simulationTime;

	private static List<Shop> shops;
	private static Queue queue;

	// metoda de citire din fisier
	public static void readFile(String fileName) {
		try {
			File file = new File(fileName);
			Scanner s = new Scanner(file);

			String data = s.nextLine();
			noClients = Integer.parseInt(data);

			String data1 = s.nextLine();
			noShops = Integer.parseInt(data1);

			String data2 = s.nextLine();
			simulationTime = Integer.parseInt(data2);

			String data3 = s.nextLine();
			String[] arrival = data3.split(",");
			min_arrival_time = Integer.parseInt(arrival[0]);
			max_arrival_time = Integer.parseInt(arrival[1]);

			String data4 = s.nextLine();
			String[] service = data4.split(",");
			min_service_time = Integer.parseInt(service[0]);
			max_service_time = Integer.parseInt(service[1]);

			s.close();
		} catch (FileNotFoundException e) {
			System.out.println("Error");
			e.printStackTrace();
		}
	}


	public static void main(String[] args) {
    String fileName = "in-test-1.txt";
    readFile(fileName);

    // Initialize shops.
    shops = new ArrayList<Shop>();
		for (int i = 1; i <= noShops; i++) {
      Shop shop = new Shop(i);
      shop.start();
			shops.add(shop);
		}

    // Initialize queue
		queue = new Queue(noClients, simulationTime, min_arrival_time, max_arrival_time, min_service_time,
				max_service_time);

		Manager manager = new Manager(simulationTime, shops, queue);
		manager.start();
	}
}
