package PT2020.demo.Tema2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Queue {

	private int max_service_time;
	private int min_service_time;
	private int max_arrival_time;
	private int min_arrival_time;
	private int noClients;
	private int simulationTime;

	private List<Client> clients;

	public Queue(int noClients,int simulationTime,int min_arrival_time,int max_arrival_time,int min_service_time,int max_service_time) {
        this.noClients=noClients;
        this.simulationTime=simulationTime;
        this.min_arrival_time=min_arrival_time;
        this.max_arrival_time=max_arrival_time;
        this.min_service_time=min_service_time;
        this.max_service_time=max_service_time;
	}

	// Generate service time and arrival time for client.
	public Client generateClient(int id) {
		Random rand = new Random();
		int serviceTime = rand.nextInt(max_service_time - min_service_time) + min_service_time;
		int arrivalTime = rand.nextInt(max_arrival_time - min_arrival_time) + min_arrival_time;
		return new Client(id, arrivalTime, serviceTime);

	}

	// Generate all clients.
	public void generateClients() {
    clients = new ArrayList<Client>();

		for (int i = 0; i < noClients; i++) {
			Client client = generateClient(i);
			clients.add(client);
		}
	}

	// Sort clients.
	public void sortClients() {
		Collections.sort(clients);
	}

	public List<Client> getClients() {
		return clients;
	}

	public String toString() {
		String message = new String();
		for (Client c : clients) {
			message = message + c.toString() + "\n";
		}

		return message;
	}
}
